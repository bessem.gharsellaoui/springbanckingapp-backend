package org.sid.sesameebankingbackend.enums;

public enum OperationType {
    DEBIT, CREDIT
}
